import React from 'react';
import ReactDOM from 'react-dom';

// import './styles.css';

class NumberControlsWithErrorMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: [
        { name: 'price', value: 0, error: '' },
        { name: 'downP', value: 0, error: '' },
        { name: 'term', value: 0, error: '' },
        { name: 'interest', value: 0, error: '' },
      ],
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(idx, e) {
    const target = e.target;
    const name = target.name;
    let error = '';

    if (isNaN(target.value)) {
      error = `${name} field can only be number`;
    }

    if (!target.value) {
      error = `${name} field cannot be empty`;
    }

    // this.state.inputs[idx] = {
    //   ...this.state.inputs[idx],
    //   value: target.value,
    //   error,
    // };

    // this.setState({
    //   inputs: [...this.state.inputs],
    // });
  }

  render() {
    return (
      <form>
        {this.state.inputs.map((input, idx) => (
          <div>
            {console.log(input)}
            <label htmlFor=''>{input.name}</label>
            <input
              type='text'
              value={input.value}
              onChange={(e) => this.handleInputChange(idx, e)}
            />
            {input.error && <span>{input.error}</span>}
          </div>
        ))}
      </form>
    );
  }
}

function App() {
  return (
    <div className='App'>
      <h1>Hello CodeSandbox</h1>
      <h2>Start editing to see some magic happen!</h2>
      <NumberControlsWithErrorMessage />
    </div>
  );
}

const rootElement = document.getElementById('root');
ReactDOM.render(<App />, rootElement);
