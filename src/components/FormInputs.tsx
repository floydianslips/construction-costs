import React from 'react';

export default function FormInput(stateConst) {
  function handleInputChange(e) {
    stateConst(e.target.value);
  }
  return (
    <tr>
      <td>
        <label htmlFor=''>Wall Spacing</label>
      </td>
      <td>
        <input
          // onSubmit={(e) => handleSubmit(e)}
          type='number'
          onChange={(e) => handleInputChange(e)}
        />
      </td>
    </tr>
  );
}
