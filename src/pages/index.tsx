import React, { state, useState } from 'react';
import styled from 'styled-components';
import FormInputs from '../components/FormInputs';
// styles
const PageStyles = styled.div`
  form {
    display: grid;
    /* grid-template-columns: repeat(2, 1fr); */
    grid-template-rows: auto;
    grid-gap: 10px;
    div {
      display: flex;
      justify-content: space-between;
      align-items: center;
      font-size: 1.5em;
    }
  }
`;

const IndexPage = () => {
  // setup state for inputs

  // state for walls (size(2x4 or 2x6), cost per board, waste %, height)
  const [gst, setGst] = useState(0.05);
  const [pst, setPst] = useState(0.07);
  const [wallSpacing, setWallSpacing] = useState(0);
  const [boardCost, setBoardCost] = useState(0);
  const [totalSheathCount, setTotalOsb] = useState(0);
  const [sheathCost, setSheathCost] = useState(0);
  const [wastePercentage, setWastePercentage] = useState(0);
  const [wallOpenings, setWallOpenings] = useState(0);
  const [twoByFour, setTwoByFour] = useState(false);
  const [twoBySix, setTwoBySix] = useState(false);
  const [wallOverallLength, setWallOverallLength] = useState(0);
  const [wallHeight, setWallHeight] = useState(0);
  const [wallOverallWidth, setWallOverallWidth] = useState(0);
  const [concreteThickness, setConcreteThickness] = useState(0);
  const [concreteCost, setConcreteCost] = useState(0);
  const [eaveOverhang, setEaveOverhang] = useState(0);
  const [roofPitch, setRoofPitch] = useState(0);
  const [roofCost, setRoofCost] = useState(0);
  const [roofScrewCost, setRoofScrewCost] = useState(0);
  //TODO add waste percentage
  // TODO add foot meter toggle
  // TODO number of screws

  // TODO fine tune roofing caculator

  // TODO state for roof (pitch, overhang, trusses)
  // state for door (#, sizes)
  const [doors, setDoors] = useState({
    quantity: null,
    size: null,
    cost: 0,
  });
  // state for windows(#, sizes)
  const [windows, setWindows] = useState({
    quantity: null,
    size: null,
    cost: null,
  });
  // currency converter
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2,
  });
  // //functions for wall inputs
  function handleWallSpacingInputChange(e) {
    const data = e.target.value;
    const re = /^[0-9\b]+$/;
    re.test(data) || data === 'e'
      ? setWallSpacing(data)
      : alert('must be a number');
    // if (e.target.value === '' || !re.test(e.target.value)) {
    //   setWallSpacing(e.target.value);
    //   alert('numbers only');
    // }
  }
  function handleWallOverallWidthInputChange(e) {
    setWallOverallWidth(parseInt(e.target.value));
  }
  function handleWallOverallLengthInputChange(e) {
    setWallOverallLength(parseInt(e.target.value));
  }
  function handleBoardCostInputChange(e) {
    setBoardCost(e.target.value);
  }
  function handleWastePercentageInputChange(e) {
    setWastePercentage(e.target.value);
  }
  function handleWallOpeningsInputChange(e) {
    setWallOpenings(e.target.value);
  }
  function handleWallHeightInputChange(e) {
    setWallHeight(e.target.value);
  }
  function handleSheathCostInputChange(e) {
    setSheathCost(e.target.value);
  }
  function handleRoofCostInputChange(e) {
    setRoofCost(e.target.value);
  }
  function handleEaveOverhangInputChange(e) {
    setEaveOverhang(parseInt(e.target.value));
  }
  function handleRoofScrewCostInputChange(e) {
    setRoofScrewCost(parseInt(e.target.value));
  }

  //functions for wall calculations
  const calculatedNumberOfBoards = (): number => {
    const studSpacing = wallSpacing / 12;
    const numberOfwidth = wallOverallWidth / studSpacing + 1;
    const numberOfLength = wallOverallLength / studSpacing + 1;
    // Assuming double headers and footers and wall height length boards
    const headersAndFooters =
      (wallOverallWidth / wallHeight) * 4 +
      (wallOverallLength / wallHeight) * 4;
    const numberOfBoards = numberOfwidth + numberOfLength + headersAndFooters;
    return numberOfBoards;
  };
  function calculateBoardCost(numberOfBoards: number, cost: number): number {
    const totalBoardsCost = numberOfBoards * cost;
    return totalBoardsCost;
  }
  const calculateSheathCount = (): number => {
    const lengthSheath = wallOverallLength * wallHeight * 2;
    const widthSheath = wallOverallLength * wallHeight * 2;
    const sheathCount = (lengthSheath + widthSheath - wallOpenings) / 32;
    return sheathCount;
  };
  function handleWallSubmit(e) {
    e.preDefault();
  }

  //functions for concrete inputs
  function handleConcreteThicknessInputChange(e) {
    setConcreteThickness(e.target.value);
  }
  function handleConcreteCostInputChange(e) {
    setConcreteCost(e.target.value);
  }
  //functions for concrete calculations
  const calculateTotalConcrete = (): number => {
    //calculate from cubic feet to cubic meters
    const totalConcrete =
      (wallOverallWidth *
        wallOverallLength *
        (concreteThickness / 12) *
        0.037) /
      1.308;
    return totalConcrete;
  };
  const calculateRoofArea = (): number => {
    const roofAngle: number =
      eaveOverhang * ((Math.atan(roofPitch) * 180) / Math.PI);
    const roofAreaCorrection = Math.sqrt(1 + (1 * roofPitch) ** 2);
    const newWidth = wallOverallWidth / 2 + eaveOverhang;
    const area = Math.sqrt(newWidth ** 2 + (newWidth * roofPitch) ** 2);
    const totalRoofing: number =
      area * wallOverallLength * 2 * roofAreaCorrection;
    return totalRoofing;
  };
  function handleRoofDropDown(e) {
    if (!e.target.matches('.dropbtn')) {
      const dropdowns = document.getElementsByClassName('dropdown-content');
      let i: number = 0;
      for (i = 0; i < dropdowns.length; i++) {
        const openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }
  const costOfConcrete = calculateTotalConcrete() * concreteCost;
  const costOfStuds = calculatedNumberOfBoards() * boardCost;
  const costOfSheathing = calculateSheathCount() * sheathCost;
  const costOfRoofing = calculateRoofArea() * roofCost;
  const numberOfRoofingScrews = calculateRoofArea() * 0.8;
  const costOfRoofingScrews = (numberOfRoofingScrews * roofScrewCost) / 100;
  const subtotal =
    costOfConcrete +
    costOfSheathing +
    costOfStuds +
    costOfRoofing +
    costOfRoofingScrews;
  const totalCost = subtotal + subtotal * pst + subtotal * gst;
  return (
    <PageStyles>
      <main>
        <h2>Walls</h2>
        <table>
          <tbody>
            <tr>
              <td>
                <label htmlFor=''>Wall Spacing</label>
              </td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleWallSpacingInputChange(e)}
                />
              </td>
            </tr>
            <tr>
              <td>
                <label htmlFor=''>Cost Per Board</label>
              </td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleBoardCostInputChange(e)}
                />
              </td>
            </tr>
            <tr>
              <td>
                <label htmlFor=''>Overall Width</label>
              </td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleWallOverallWidthInputChange(e)}
                />
              </td>
            </tr>
            <tr>
              <td>
                <label htmlFor=''>Overall Length</label>
              </td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleWallOverallLengthInputChange(e)}
                />
              </td>
            </tr>
            <tr></tr>
            <tr>
              <td>
                <label htmlFor=''>Wall Openings</label>
              </td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleWallOpeningsInputChange(e)}
                />
              </td>
            </tr>
            <tr>
              <td>
                <label htmlFor=''>Sheathing Cost</label>
              </td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleSheathCostInputChange(e)}
                />
              </td>
            </tr>

            <tr>
              <td>
                <label htmlFor=''>Wall Height</label>
              </td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleWallHeightInputChange(e)}
                />
              </td>
            </tr>
            <tr>
              <td>Number of Boards</td>
              <td>
                {!wallOverallLength ||
                !wallOverallWidth ||
                !wallSpacing ||
                !wallHeight
                  ? 0
                  : Math.ceil(calculatedNumberOfBoards())}
              </td>
            </tr>
            <tr>
              <td>Total Sheathing</td>
              <td>{Math.ceil(calculateSheathCount())}</td>
            </tr>
          </tbody>
        </table>
        <h2>Concrete</h2>
        <table>
          <tbody>
            <tr>
              <td>
                <label htmlFor=''>Concrete Thickness (in Inches)</label>
              </td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleConcreteThicknessInputChange(e)}
                />
              </td>
            </tr>
            <tr>
              <td>
                <label htmlFor=''>Concrete Price (per m3)</label>
              </td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleConcreteCostInputChange(e)}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <h2>Roofing</h2>
        <table>
          <tbody>
            <tr>
              <td>Roof Pitch</td>
              <td>
                <select
                  name='roofing'
                  id='roof'
                  onChange={(e) => setRoofPitch(parseInt(e.target.value) / 12)}
                >
                  <option value='1'>1/12</option>
                  <option value='2'>2/12</option>
                  <option value='3'>3/12</option>
                  <option value='4'>4/12</option>
                  <option value='5'>5/12</option>
                  <option value='6' selected>
                    6/12
                  </option>
                  <option value='7'>7/12</option>
                  <option value='8'>8/12</option>
                  <option value='9'>9/12</option>
                  <option value='10'>10/12</option>
                  <option value='11'>11/12</option>
                  <option value='12'>12/12</option>
                  <option value='13'>13/12</option>
                  <option value='14'>14/12</option>
                  <option value='15'>15/12</option>
                  <option value='16'>16/12</option>
                  <option value='17'>17/12</option>
                  <option value='18'>18/12</option>
                  <option value='19'>19/12</option>
                  <option value='20'>20/12</option>
                  <option value='21'>21/12</option>
                  <option value='22'>22/12</option>
                  <option value='23'>23/12</option>
                  <option value='24'>24/12</option>
                </select>
              </td>
            </tr>

            <tr>
              <td>Eaves Stick Out</td>
              <td>
                <input
                  // onSubmit={(e) => handleSubmit(e)}
                  type='number'
                  onChange={(e) => handleEaveOverhangInputChange(e)}
                />
              </td>
            </tr>
            <tr>
              <td>Total Area</td>
              <td>{Math.ceil(calculateRoofArea())}</td>
            </tr>
            <tr>
              <td>Roofing Cost per sqft</td>
              <td>
                {
                  <input
                    // onSubmit={(e) => handleSubmit(e)}
                    type='number'
                    onChange={(e) => handleRoofCostInputChange(e)}
                  />
                }
              </td>
            </tr>
            <tr>
              <td>Roofing Screw Cost per 100</td>
              <td>
                {
                  <input
                    // onSubmit={(e) => handleSubmit(e)}
                    type='number'
                    onChange={(e) => handleRoofScrewCostInputChange(e)}
                  />
                }
              </td>
            </tr>
            <tr>
              <td>Number of Screws</td>
              <td>{Math.ceil(numberOfRoofingScrews)}</td>
            </tr>
          </tbody>
        </table>
        <h2>Totals</h2>
        <table>
          <tbody>
            <tr>
              <td>Cost of Boards</td>
              <td>
                {!calculatedNumberOfBoards() || !boardCost
                  ? formatter.format(0)
                  : formatter.format(calculatedNumberOfBoards() * boardCost)}
              </td>
            </tr>
            <tr>
              <td>Cost of Concrete (not counting surcharges)</td>
              <td>{formatter.format(costOfConcrete)}</td>
            </tr>
            <tr>
              <td>Cost of Sheathing</td>
              <td>{formatter.format(calculateSheathCount() * sheathCost)}</td>
            </tr>
            <tr>
              <td>Total Cost Roofing</td>
              <td>{formatter.format(costOfRoofing)}</td>
            </tr>
            <tr>
              <td>Cost Roofing Screws</td>
              <td>{formatter.format(costOfRoofingScrews)}</td>
            </tr>
            <tr>
              <td>Subtotal</td>
              <td>
                {!(
                  costOfConcrete +
                  costOfSheathing +
                  costOfStuds +
                  costOfRoofing
                )
                  ? formatter.format(0)
                  : formatter.format(subtotal)}
              </td>
            </tr>
            <tr>
              <td>PST (BC) @ 7%</td>
              <td>
                {!subtotal
                  ? formatter.format(0)
                  : formatter.format(subtotal * pst)}
              </td>
            </tr>
            <tr>
              <td>GST @ 5%</td>
              <td>
                {!subtotal
                  ? formatter.format(0)
                  : formatter.format(subtotal * gst)}
              </td>
            </tr>
            <tr>
              <td>Total</td>
              <td>
                {!subtotal ? formatter.format(0) : formatter.format(totalCost)}
              </td>
            </tr>
          </tbody>
        </table>
      </main>
    </PageStyles>
  );
};

export default IndexPage;
